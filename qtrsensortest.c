/****************************************************************************/
/* Supported Microcontroller:RX62T                                          */
/* File contents:   Searching the maximum steering angle from PC            */
/*                  (RX62T version)                                         */
/* Version:     Ver.1.00                                                    */
/* Date:        2013.09.04                                                  */
/* Copyright:   Renesas Micon Car Rally Secretariat                         */
/****************************************************************************/
/*

*/

/*======================================*/
/* Include                              */
/*======================================*/
#include "iodefine.h"
#include <machine.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "typedefine.h"
#include "printf_lib.h"                 // printf use library
#include "cmt.h"
#include "qtrsensors.h"

#define API_MEASURETIME 1
#define API_FASTPOLLING 2

/* Select what API you'd like to test */
//#define API API_MEASURETIME
#define API API_FASTPOLLING

/* This is used by the fast-polling API. */
/* You can determine what value is right for you by testing the measure-time API. */
#define MEASURE_US 500 

/* This is used by the fast-polling API. */
/* We like to poll every 100ms here, but you can use whatever value */
#define IDLE_US (100000 - QT_CHARGE_US - MEASURE_US) 

void init(void);

/*======================================*/

void print_mask(uint32_t mask)
{
    size_t i;
    char str[QTR_SENSORCOUNT + 1] = { 0 };
    for (i = 0; i < QTR_SENSORCOUNT; ++i) {
        str[i] = (mask & (1 << i)) ? '1' : '0';
    }
    printf("%s", str);
}

#if API == API_FASTPOLLING
void cmt0_user(void){
    static uint32_t qtrtimer_us;
    
    if (qtrtimer_us <= CMT0_PERIOD_US) {
        qtrtimer_us = qtr_update(MEASURE_US, IDLE_US);
    } else {
        qtrtimer_us -= CMT0_PERIOD_US;
    }
}
#elif API == API_MEASURETIME
void cmt0_user(void){
    /* */
}
#endif

void main(void)
{
    init();    
    cmt0_init();

    init_sci1_printf( SPEED_9600 );     // SCI1 and printf initialization
    setpsw_i();                         // Ibit=1(permit interrupt),IPL=0

    printf("hello\n");

#if API == API_FASTPOLLING
    for (;;) {
    	uint32_t mask;
        if (qtr_poll(&mask)) {
            printf("frontsensors ");
            print_mask(mask);
            printf("\n");
        }
    }
#elif API == API_MEASURETIME
    for (;;) {
        qtr_results_t measured;
        size_t i;
        
    	measured = qtr_measure();

        for (i = 0; i < QTR_SENSORCOUNT; ++i) {
            printf("%5lu ", measured.time_us[i]);
        }
        printf("\n");
    
        cmt0_delay_ms(100);
    }
#endif
}

/************************************************************************/
/* RX62T  Initialization                                                */
/************************************************************************/
void init( void )
{
    // System Clock
    SYSTEM.SCKCR.BIT.ICK = 0;               //12.288*8=98.304MHz
    SYSTEM.SCKCR.BIT.PCK = 1;               //12.288*4=49.152MHz

    // Port I/O Settings
    PORT1.DDR.BYTE = 0x02;                  //P10:LED2 in motor drive board

    PORT2.DR.BYTE  = 0x08;
    PORT2.DDR.BYTE = 0x1b;                  //P24:SDCARD_CLK(o)
                                            //P23:SDCARD_DI(o)
                                            //P22:SDCARD_DO(i)
                                            //CN:P21-P20
    PORT3.DR.BYTE  = 0x01;
    PORT3.DDR.BYTE = 0x0f;                  //CN:P33-P31
                                            //P30:SDCARD_CS(o)

    PORT7.DDR.BYTE = 0x7e;                  //P76:LED3 in motor drive board
                                            //P75:forward reverse signal(right motor)
                                            //P74:forward reverse signal(left motor)
                                            //P73:PWM(right motor)
                                            //P72:PWM(left motor)
                                            //P71:PWM(servo motor)
                                            //P70:Push-button in motor drive board
    PORT8.DDR.BYTE = 0x07;                  //CN:P82-P80
    PORTA.DR.BYTE  = 0x0f;                  //CN:PA5-PA4
                                            //PA3:LED3(o)
                                            //PA2:LED2(o)
                                            //PA1:LED1(o)
                                            //PA0:LED0(o)
    PORTA.DDR.BYTE = 0x3f;                  //CN:PA5-PA0
    PORTD.DDR.BYTE = 0x07;                  //PD7:TRST#(i)
                                            //PD5:TDI(i)
                                            //PD4:TCK(i)
                                            //PD3:TDO(o)
                                            //CN:PD2-PD0
    PORTE.DDR.BYTE = 0x1b;                  //PE5:SW(i)
                                            //CN:PE4-PE0
}

/************************************************************************/
/* end of file                                                          */
/************************************************************************/

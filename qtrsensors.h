#ifndef INC_QTRSENSORS_H
#define INC_QTRSENSORS_H

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "cmt.h"

#define QTR_SENSORCOUNT 16

/* +++++ Low-Level API +++++ */

#define QTR_CHARGE_US      UINT32_C(10)
#define QTR_MEASURE_MAX_US UINT32_C(10000)

/* Sets all sensor pins to output mode */
void qtr_outputmode(void);

/* Sets all sensor pins to high voltage */
void qtr_sethigh(void);

/* Sets all sensor pins to input mode */
void qtr_inputmode(void);

/* Reads all sensor pins */
uint32_t qtr_read(void);

/* output mode -> high voltage -> delay_us(10) -> input mode */
void qtr_charge(void);

/* +++++ Measuring API +++++ */

typedef struct {
    uint32_t time_us[QTR_SENSORCOUNT];
} qtr_results_t;

/* Measures the time (in microseconds) it takes for all sensor pins to fall to low voltage after charging up. 
 *
 * Manages global state and may not be used in conjunction with qtr_update().
 */
qtr_results_t qtr_measure(void);

/* +++++ Fast-Polling API +++++ */

/* Checks whether there is new sensor data available, if so writes it to mask and returns true.
 * Returns false otherwise.
 *
 * Must be used in conjunction with qtr_update().
 */
bool qtr_poll(uint32_t * mask);

/* Repeatedly charges the sensors and provides new sensor data for polling when ready.
 *
 * This functions is non-blocking and expects its caller to manage it's timing correctly.
 * It returns the time in microseconds in which it expects to be called again.
 *
 * Manages global state and may not be used in conjunction with qtr_measure().
 */
uint32_t qtr_update(uint32_t measure_us, uint32_t idle_us);

#endif /* INC_QTRSENSORS_H */

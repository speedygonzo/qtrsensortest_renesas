#ifndef INC_CMT_H
#define INC_CMT_H

#include <stdint.h>

/* Convert microseconds to milliseconds and back */
#define US_TO_MS(us) ((us) / UINT32_C(1000))
#define MS_TO_US(ms) ((ms) * UINT32_C(1000))

/* The smallest delay time we can use on CMT0 */
#define CMT0_PERIOD_US UINT32_C(10)

/* Delay execution for a number of microseconds, should be a multiple of
 * CMT0_PERIOD_US */
void cmt0_delay_us(uint32_t us);

/* Delay execution for a number of milliseconds, must be less-equal than
 * (UINT32_MAX / CMT0_PERIOD_US) */
void cmt0_delay_ms(uint32_t ms);

/* This functions needs to be called before other cmt0_* functions are called */
void cmt0_init(void);

/* This function needs to be provided by the user of the library, will be called
 * periodically */
extern void cmt0_user(void);

#endif /* INC_CMT_H */

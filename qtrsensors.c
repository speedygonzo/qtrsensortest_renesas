#include "qtrsensors.h"
#include "iodefine.h"
#include "limits.h"

#define QTR_PORTB_MASK 0xFF // PB0-PB7
#define QTR_PORT9_MASK 0x7F // P90-P96
#define QTR_PORT1_MASK 0x02 // P11

#define QTR_PORTB_IDX 0
#define QTR_PORT9_IDX 8
#define QTR_PORT1_IDX 14

/* CN5       <-> PORTB */
/* CN7.P3-P9 <-> PORT9.B0-B6 */
/* CN7.P2    <-> PORT1.B0 */

void qtr_outputmode(void)
{
    PORTB.DDR.BYTE |= QTR_PORTB_MASK;
    PORT9.DDR.BYTE |= QTR_PORT9_MASK;
    PORT1.DDR.BYTE |= QTR_PORT1_MASK;
}

void qtr_sethigh(void)
{
    PORTB.DR.BYTE |= QTR_PORTB_MASK;
    PORT9.DR.BYTE |= QTR_PORT9_MASK;
    PORT1.DR.BYTE |= QTR_PORT1_MASK;
}

void qtr_inputmode(void)
{
    PORTB.DDR.BYTE &= ~QTR_PORTB_MASK;
    PORT9.DDR.BYTE &= ~QTR_PORT9_MASK;
    PORT1.DDR.BYTE &= ~QTR_PORT1_MASK;
}

uint32_t qtr_read(void)
{
	uint8_t portb = PORTB.PORT.BYTE & QTR_PORTB_MASK;
    uint8_t port9 = PORT9.PORT.BYTE & QTR_PORT9_MASK;
    uint8_t port1 = PORT1.PORT.BYTE & QTR_PORT1_MASK;
    return (port1 << QTR_PORT1_IDX) | (port9 << QTR_PORT9_IDX) | (portb << QTR_PORTB_IDX);
}

void qtr_charge(void)
{
    qtr_outputmode();
    qtr_sethigh();
    cmt0_delay_us(QTR_CHARGE_US);
    qtr_inputmode();
}

qtr_results_t qtr_measure()
{
	qtr_results_t results;
    size_t i;
	uint32_t mask;
	uint32_t time_us = 0;

    for (i = 0; i < QTR_SENSORCOUNT; ++i) {
        results.time_us[i] = QTR_MEASURE_MAX_US;
    }

    qtr_charge();
    
    do {
        mask = qtr_read();
        for (i = 0; i < QTR_SENSORCOUNT; ++i) {
            if (results.time_us[i] == QTR_MEASURE_MAX_US && (mask & (1 << i)) == 0) {
                results.time_us[i] = time_us;
            }
        }

        cmt0_delay_us(CMT0_PERIOD_US);
        time_us += CMT0_PERIOD_US;
    } while (mask && time_us <= QTR_MEASURE_MAX_US);

    return results;
}

#define QTR_NOTREADY UINT32_MAX
static volatile uint32_t g_mask = QTR_NOTREADY;

bool qtr_poll(uint32_t * out)
{
    uint32_t mask = g_mask;
    g_mask = QTR_NOTREADY;
    *out = mask;   
    return mask != QTR_NOTREADY;
}

uint32_t qtr_update(uint32_t measure_us, uint32_t idle_us)
{
    static enum { QTR_IDLE, QTR_CHARGING, QTR_MEASURING } state = QTR_IDLE;
   
    switch (state) {
    case QTR_CHARGING:
        qtr_inputmode();
        state = QTR_MEASURING;
        return measure_us;
       
    case QTR_MEASURING:
        g_mask = qtr_read();
        state = QTR_IDLE;
        return idle_us;
       
    case QTR_IDLE:
        qtr_outputmode();
        qtr_sethigh();
        state = QTR_CHARGING;
        return QTR_CHARGE_US;
    }

    return 0;
}

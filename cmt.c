#include "cmt.h"
#include "iodefine.h"

volatile int init0;     // 1 if cmt0_init() was called, 0 otherwise
volatile uint32_t cnt0; // unit: CMT0_PERIOD_US

#pragma interrupt Excep_CMT0_CMI0(vect=VECT_CMT0_CMI0)
void Excep_CMT0_CMI0(void)
{
    if (!init0) return;
    cnt0++;
    cmt0_user();
}

void cmt0_delay_us(uint32_t us)
{
    cnt0 = 0;
    while (cnt0 < (us / CMT0_PERIOD_US));
}

void cmt0_delay_ms(uint32_t ms)
{
    cmt0_delay_us(MS_TO_US(ms));
}

void cmt0_init(void)
{
    // ## POWER ON
    // See r_cmt_rx.c:power_on:L543

    MSTP(CMT0) = 0;          /* Bring module out of stop state. */
    CMT.CMSTR0.BIT.STR0 = 0; /* Stop counter. */

    // ## SETUP CHANNEL
    // See r_cmt_rx.c:cmt_setup_channel:L814

    #define FREQ_HZ 100000        /* Desired frequency (0.1MHz = 10us) */
    #define CMT_PCLK_HZ 49152000  /* Peripheral module clock */

    CMT0.CMCR.BIT.CKS = 0; /* Set clock divisor 8 to be used. */
    CMT0.CMCOR = (uint16_t) ((uint32_t) CMT_PCLK_HZ / 8 / FREQ_HZ);

    // ## START COUNTER
    // See r_cmt_rx.c:cmt_counter_start:L680

    CMT0.CMCR.BIT.CMIE = 1; /* Enable compare match interurpt. */
    CMT0.CMCNT = 0;         /* Clear counter. */

    IPR(CMT0, CMI0) = 15;    /* Set interrupt priority */ // TODO why 15? search for CMT_RX_CFG_IPR
    IEN(CMT0, CMI0) = 1;     /* Enable compare match interrupt */
    CMT.CMSTR0.BIT.STR0 = 1; /* Start counter. */

    init0 = 1;
}
